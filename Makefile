## please modify the following line for naming the end products (PDFs, ZIPs, ...)
PROJECTNAME = "KSN-Fragen"

## -----------------------------------------
##       DO NOT EDIT BELOW THIS LINE
## -----------------------------------------

## Makefile von Karl Voit (Karl@Voit.net)
## Edited by Emanuel Oberholzer (emanueloberholzer400@gmail.com)

## some Makefile-hints taken from: 
## http://www.ctan.org/tex-archive/help/uk-tex-faq/Makefile

MAINDOCUMENTBASENAME = main
MAINDOCUMENTFILENAME = ${MAINDOCUMENTBASENAME}.tex
## COMMANDS:
PDFLATEX_CMD = pdflatex
#BIBTEX_CMD = bibtex
BIBTEX_CMD = biber
MAKEIDX_CMD = makeindex
DATESTAMP = `/bin/date +%Y-%m-%d`
PDF_NAME = ${DATESTAMP}_${PROJECTNAME}
PDFVIEWER = evince
TEMPLATEDOCUBASENAME = Template-Documentation
TEMPLATEDOCUFILE = ${TEMPLATEDOCUBASENAME}.tex


#help
#helpThe main targets of this Makefile are:
#help	help	this help
.PHONY: help
help:
	@sed -n 's/^#help//p' < Makefile

#help	all	see "pdf"
.PHONY: all
all: pdf

#help	all-quick	see pdf-quick
.PHONY: all-quick
all-quick: pdf-quick

#help	pdf	creates a pdf file using pdflatex
.PHONY: pdf
pdf:
	${PDFLATEX_CMD} ${MAINDOCUMENTFILENAME}
	-${BIBTEX_CMD} ${MAINDOCUMENTBASENAME}
	${PDFLATEX_CMD} ${MAINDOCUMENTFILENAME}
	${PDFLATEX_CMD} ${MAINDOCUMENTFILENAME}
	@cp ${MAINDOCUMENTBASENAME}.pdf ${PDF_NAME}.pdf

#help 	pdf-quick 	only uses pdflatex to compile the document
.PHONY: pdf-quick
pdf-quick:
	${PDFLATEX_CMD} ${MAINDOCUMENTFILENAME}
	@cp ${MAINDOCUMENTBASENAME}.pdf ${PDF_NAME}.pdf

#help	wc	counts the words from the PDF generated
wc:	all
	pdftops ${PDF_NAME}.pdf
	ps2ascii ${PDF_NAME}.ps > ${PDF_NAME}.txt
	wc -w ${PDF_NAME}.txt

# --------------------------------------------------------

#help	view	view the PDF-file
.PHONY: view
view: all
	${PDFVIEWER} ${PDF_NAME}.pdf

#help	view-quick	view the PDF-file
.PHONY: view-quick
view-quick: all-quick
	${PDFVIEWER} ${PDF_NAME}.pdf

# --------------------------------------------------------

#help	clean	clean up temporary files
.PHONY: clean
clean: 
	@latexmk -C
	@rm -r *.bcf *.run.xml _*_.* *~ *.aux *-blx.bib *.bbl ${MAINDOCUMENTBASENAME}.dvi *.ps *.blg *.idx *.ilg *.ind *.toc *.log *.log *.brf *.out *.lof *.lot *.gxg *.glx *.gxs *.glo *.gls *.tdo *${PROJECTNAME}.txt -f *.lol

#help	purge	cleaner than clean ;-)
.PHONY: purge
purge: clean
	@rm 20*.pdf *.ps -f

#help	force	force rebuild next run
.PHONY: force
force:
	@touch *tex

# TOOLS:

#help	zip	create ZIP-file
.PHONY: zip
zip: purge pdf clean
	zip -r ../${PROJECTNAME}_${TIMESTAMP}.zip *

.PHONY: publish
publish: templatedocu pdf clean
	@rm 20*.pdf ${TEMPLATEDOCUFILE} -f
	@git status

#help	templatedocu	updates tex-files for the documentation of this template
#help			needs: echo, sed, grep
.PHONY: templatedocu
templatedocu:
	@grep "%doc%" template/preamble.tex | sed 's/^.*%doc% //' > ${TEMPLATEDOCUFILE}
	@grep "%doc%" template/mycommands.tex | sed 's/^.*%doc% //' >> ${TEMPLATEDOCUFILE}
	@grep "%doc%" template/typographic_settings.tex | sed 's/^.*%doc% //' >> ${TEMPLATEDOCUFILE}
	@grep "%doc%" template/pdf_settings.tex | sed 's/^.*%doc% //' >> ${TEMPLATEDOCUFILE}
	@echo "%%---------------------------------------%%" >>${TEMPLATEDOCUFILE}
	@echo "\printbibliography\end{document}" >>${TEMPLATEDOCUFILE}
	${PDFLATEX_CMD} ${TEMPLATEDOCUFILE}
	${PDFLATEX_CMD} ${TEMPLATEDOCUFILE}
	${BIBTEX_CMD} ${TEMPLATEDOCUBASENAME}
	${PDFLATEX_CMD} ${TEMPLATEDOCUFILE}

#--------------------------------------------------------

#help 	install_autocompletion	install autocompletion for custom commands
.PHONY: install_autocompletion
install_autocompletion:
	@./scripts/install_autocompletion.sh

