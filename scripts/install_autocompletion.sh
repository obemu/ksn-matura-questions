#! /bin/bash

# Script for simple usage of the brscan-skey tool
# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# shellcheck disable=SC2155

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="install_autocompletion"
readonly AUTOCOMPLETION_DIR="autocompletion/"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#   4. Extra code to evaluate before exiting(=$4)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $2"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    echo -e "${TEXT_STYLE_FG_DEBUG}DEBUG:${TEXT_STYLE_END} $2"
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $2"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $2"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  echo "This script copies the files from $AUTOCOMPLETION_DIR in the"
  echo "project directory of this diploma thesis into the "
  echo "users configuration directory for the specified IDE, in order "
  echo "to enable the autocompletion feature for the custom functions "
  echo "in the project."
  echo
  echo "You can also uninstall the file from the users configuration "
  echo "directory."

  exit 0
}

# PARAMETERS
#   1. install (true) or uninstall (false)
#   2. Project directory
function installTeXstudio() {
  local userDir="$HOME/.config/texstudio/completion/user"
  local install=$1
  local projectAutocompletionDir="$2/$AUTOCOMPLETION_DIR"

  # Check if userDir exists.
  if ! [ -d "$userDir" ]; then
    # Cannot uninstall something that does not exist
    if [ "false" = "$install" ]; then
      log 3 "The directory '$userDir' does not exist, therefore there is nothing that can be uninstalled."
      return
    fi

    log 5 101 "The directory '$userDir' does not exist"
  fi

  # Iterate over every *.cwl file in $projectAutocompletionDir .
  find "$projectAutocompletionDir" -type f -iname "*.cwl" | while read -r file; do

    # Uninstall files from the projects $AUTOCOMPLETION_DIR .
    if [ "false" = "$install" ]; then
      local installedFile="$userDir/$(basename "$file")"

      if ! [ -e "$installedFile" ]; then
        log 1 "The file '$installedFile' does not exist, therefore there is nothing that can be uninstalled."
        return
      fi

      rm -f "$installedFile"
      log 3 "Uninstalled '$installedFile'."

    # Install files from the projects $AUTOCOMPLETION_DIR.
    else
      cp "$file" "$userDir"
      local status=$?

      if [ 0 -eq $status ]; then
        log 3 "Successfully copied '$file' to '$userDir'."
      else
        log 5 102 "Failed to copy '$file' to '$userDir'."
      fi

    fi

  done
}

######################## Script ########################

info

echo "Choose what you want to do. Default is install."
echo "1) Install"
echo "2) Uninstall"
echo "3) Help"
read -r chosenMode

case $chosenMode in
1) shouldInstall="true" ;;
2) shouldInstall="false" ;;
3) help ;;
*) shouldInstall="true" ;;
esac

## Get the path to the project directory.
projectDir=$(pwd)

echo -n "Please enter the path to the directory of the this "
echo -n "diploma thesis project. Or press enter to continue and use "
echo -n "the current directory ('$projectDir')."
read -r inputProjectDir

if [ -n "$inputProjectDir" ]; then
  projectDir=$inputProjectDir
fi

if ! [ -d "$projectDir" ]; then
  log 5 1 "The directory '$projectDir' does not exist."
fi

## Find out the useres ide.
echo "Choose your LaTeX IDE."
echo "1) TeXstudio"
read -r chosenIde

case $chosenIde in
1) installTeXstudio $shouldInstall "$projectDir" ;;
*) help ;;
esac

exit 0
