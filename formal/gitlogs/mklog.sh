#!/bin/bash

fabs="micyp@protonmail.ch"
steve="wagayuuri@gmail.com"
rohry="fabro122@hotmail.com"

name=$(eval "echo \$$1")

git log --reverse --author $name "--pretty=format:%at^%s"  | while IFS= read -r LINE; do ts=$(cut -d'^' -f1 <<<"$LINE"); printf "$(date --date=@$ts +"%V") & $(cut -d'^' -f2 <<<"$LINE" | sed 's/&/\\&/g')"; echo "\\\\"; done \
	| sed 's/_/\\_/g' | sed 's/°/\\textdegree/g' > $1.tex
