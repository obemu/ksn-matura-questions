# WHY

Figures come from somewhere. If you draw them using draw.io, save the draw.io export files here. NOT just the pngs.
If you use any other software, say, LibreOffice Draw, save the document files here.
The actual files used in the document should be generated from those in this directory tree.

