# For a detailed explanation of .cwl files see 
# https://htmlpreview.github.io/?https://github.com/texstudio-org/texstudio/master/utilities/manual/usermanual_en.html#CWLDESCRIPTION

\namedref{name}{label}#r

\fullref{label}#r
\seeref{label}#r
\seerefbr{label}#r

\shortref{label}#r
\seeshortref{label}#r
\seeshortrefbr{label}#r

\bookpar{name}{bibid}#c
\seebookpar{name}{bibid}#c
\seebookparbr{name}{bibid}#c

\longcite{bibid}#c
\seelongcite{bibid}#c
\seelongcitebr{bibid}#c

